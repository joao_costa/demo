

# ------------------------------------------------------------------
# Configuration file helpers for cx
# ------------------------------------------------------------------

# IMPORTS
import os
import requests
import json
import xml.dom.minidom



# ------------------------------------------------------------------
# usage:    retrieves the engine cxversion using REST
#           the version returned will be the one found on the 
#           first readable engine
#           usable form v8.5.0, using either REST or SOAP
# hostname: can be the host or the ip address
#           can be prefixed with "http://" or "https://""
#           can be suffixed with a port number in the form ":port"
#           if not prefixed then http:// is assumed
# apitoken: a valid Bearer token. (see: cxauthy.py)
#           when present, version will be retrieved via REST
#           call to engine servers information
#           when missing, version will be retrieved using soap 
#           (a deprecated mode)
# returns:  engine version, if found
# ------------------------------------------------------------------
def cxversion( hostname = "", apitoken = "" ):

    sversion = ""

    if (hostname == ""):
        raise Exception("hostname is missing")

    sapipath = hostname.lower()

    if ( sapipath.startswith('http://') == False ) and ( sapipath.startswith('https://') == False ) :
        sapipath = "http://" + sapipath

    # When api token is missing, go for soap
    if (apitoken == ""):

        if ( sapipath.endswith( '/') ) :
            sapipath = sapipath + "CxWebInterface/Portal/CxWebService.asmx"
        else :
            sapipath = sapipath + "/CxWebInterface/Portal/CxWebService.asmx"

        # Setup soap envelope
        ssoap = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:chec="http://Checkmarx.com">'
        ssoap = ssoap + '<soapenv:Header/>'
        ssoap = ssoap + '<soapenv:Body>'
        ssoap = ssoap + '<chec:GetVersionNumber/>'
        ssoap = ssoap + '</soapenv:Body>'
        ssoap = ssoap + '</soapenv:Envelope>'

        #ssoap = ssoap.encode('utf-8')

        shead = {'Content-Type':'text/xml;charset=UTF-8',
                'Content-Length':str(len(ssoap)),
                'SOAPAction':'http://Checkmarx.com/GetVersionNumber' }

        # Post request to host, accepting self-signed certificates
        sresponse = requests.post( sapipath, data = ssoap, headers = shead, verify = False )

        if (sresponse.status_code not in [200, 201, 202]):
            return ""

        # Process result xml
        sxml    = xml.dom.minidom.parseString( sresponse.content.decode( 'utf-8' ) )
        snodes  = sxml.getElementsByTagName("Version")

        if (snodes != None):
            sversion = snodes[0].firstChild.nodeValue

        if (sversion == "") or (sversion == None):
            sversion = ""    
        elif (sversion.startswith("V ")):
                sversion = sversion[2:]

    else:

        if ( sapipath.endswith( '/') ) :
            sapipath = sapipath + "cxrestapi/sast/engineServers"
        else :
            sapipath = sapipath + "/cxrestapi/sast/engineServers"

        shead = {'Content-Type':'application/json',
            'Authorization':apitoken }

        sbody = {}

        # Get request to host, accepting self-signed certificates
        sresponse = requests.get( sapipath, data = sbody, headers = shead, verify = False )
        
        if (sresponse.status_code not in [200, 201, 202]):
            return ""

        # Process result json
        sengines = json.loads(sresponse.content)
        for sengine in sengines :
            sversion = sengine.get("cxVersion")
            if (sversion == "") or (sversion == None):
                sversion = ""

    return sversion
   
    

